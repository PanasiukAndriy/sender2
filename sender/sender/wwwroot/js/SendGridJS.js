﻿var form = document.getElementsByTagName('form')[0];
var submit = document.getElementById('submit');
var loader = document.getElementById('loader');

function sendRequest() {
    var firstname = document.getElementById('first_name');
    var lastname = document.getElementById('last_name');
    var email = document.getElementById('email');

    var body = JSON.stringify({
        first_name: firstname && firstname.value.trim() ? firstname.value.trim() : undefined,
        last_name: lastname && lastname.value.trim() ? lastname.value.trim() : undefined,
        email: email.value.trim(),
        //recaptcha: grecaptcha.getResponse()
    });

    // use xmlhttprequest to better support IE
    var xhr = new XMLHttpRequest();

    xhr.open('POST', 'api/homeapi');
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.timeout = 10000; // time in milliseconds

    xhr.onreadystatechange = function () {
        if (this.readyState === XMLHttpRequest.DONE) {
            // Status can be zero if the req failed to send
            if (this.status < 200 || this.status >= 400) {
                handleError(this.status);
            } else {
                handleSuccess();
            }
        }
    }

    xhr.send(body);
}

function startLoading() {
    form.removeEventListener('submit', handleSubmit);
    submit.disabled = true;

    loader.classList.remove('hidden');
    submit.classList.add('loading');
}

function handleError(status) {
    var email = document.getElementById('email').value;
    var errorId = 'error-message';
    if (!/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+[.]+[a-zA-Z]{2,6}$/.test(email)) {
        // show an error if the email input value is invalid
        errorId = 'invalid-email-error-message';
    //} else if (!grecaptcha.getResponse()) {
    //    // recaptcha was not checked
    //    errorId = 'recaptcha-error-message';
    } else if (status < 200) {
        // a status < 200 means the request has timed-out
        errorId = 'timeout-error-message';
    }

    form.addEventListener('submit', handleSubmit);
    submit.disabled = false;

    var errorMessage = document.getElementById(errorId);
    errorMessage.classList.remove('hidden');

    loader.classList.add('hidden');
    submit.classList.remove('loading');
}

function handleSubmit(e) {
    e.preventDefault();

    startLoading();
    sendRequest();
}

function handleSuccess() {
    var confirmation = document.getElementById('confirmation-message');

    // we should keep the form the same size but hide the inputs and show conf message
    var formChildren = Array.prototype.slice.call(form.children);
    for (var i = 0; i < formChildren.length; i++) {
        var child = formChildren[i];
        if (child === confirmation) {
            child.classList.remove('hidden');
        } else {
            child.classList.add('no-visibility');
        }
    }
}

form.addEventListener('submit', handleSubmit);
