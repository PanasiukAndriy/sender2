﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using sender.Models;
using SendGrid;

namespace sender.Controllers
{

	[ApiController]
	public class HomeApiController : ControllerBase
	{
		private readonly IOptions<Config> _config;

		private static SendGridClient _client;

		private readonly string _receiptsURL = "contactdb/recipients";

		private readonly string _addReceiptsToListURL = "contactdb/lists/{0}/recipients/(1)";

		public HomeApiController(IOptions<Config> config)
		{
			_config = config;
			_client = new SendGridClient(config.Value?.APIkey);
		}

		// POST: api/HomeApi
		[HttpPost("api/HomeApi")]
		public async System.Threading.Tasks.Task<IActionResult> PostAsync(User user)
		{
			if (!ModelState.IsValid)
			{
				return null;
			}

			// Add recipients
			string data = JsonConvert.SerializeObject(new List<User> { user });

			var response = await _client.RequestAsync(method: SendGridClient.Method.POST, urlPath: _receiptsURL, requestBody: data);
			if (response.StatusCode == HttpStatusCode.BadRequest)
			{
				return BadRequest();
			}

			var result = JsonConvert.DeserializeObject<ReceiptsResponse>(response.Body.ReadAsStringAsync().Result);

			// Add a Single Recipient to a List
			response = await _client.RequestAsync(method: SendGridClient.Method.POST, urlPath: string.Format(_addReceiptsToListURL,_config.Value?.ListId, result.persisted_recipients.FirstOrDefault()));
			if (response.StatusCode == HttpStatusCode.BadRequest)
			{
				return BadRequest();
			}

			return Ok();
		}
	}
}
