﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace sender.Models
{
	public class User
	{
		[JsonProperty("first_name")]
		public string first_name { get; set; }

		[JsonProperty("last_name")]
		public string last_name { get; set; }

		[EmailAddress]
		[JsonProperty("email")]
		public string Email { get; set; }
	}
}
