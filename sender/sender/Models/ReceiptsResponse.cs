﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sender.Models
{
	public class ReceiptsResponse
	{
		public int new_count { get; set; }
		public int updated_count { get; set; }
		public int error_count { get; set; }
		public List<object> error_indices { get; set; }
		public List<int> unmodified_indices { get; set; }
		public List<string> persisted_recipients { get; set; }
		public List<object> errors { get; set; }
	}
}
